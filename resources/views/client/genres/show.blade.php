@extends('layouts.app')

@section('content')
    <div class="container">
        <br>
        <form action="{{action([App\Http\Controllers\GenresController::class, 'index'])}}">
            <button class="btn btn-primary">Назад</button>
        </form>
                <br>
                <b>Книги в жанре: {{$genre->name}}</b>
                <br>
                <br>
        <div class="row">
            @foreach($genre->books as $book)
                {{--                @if($book->genre_id == $genre->id)--}}
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="{{asset('/storage/' . $book->picture)}}" alt="{{$book->picture}}">
                    <div class="card-body">
                        <h5 class="card-title">{{$book->title}}</a></h5>
                            <br>
                            @if(!is_null($book->author))
                                {{$book->author->name}}
                            @endif
                        <br>
                        <a href="{{route('client.orders.create', ['book' => $book])}}">

                            <button class="btn btn-primary" type="submit">Получить книгу {{$book->title}}</button>
                        </a>
{{--                        <button class="btn btn-primary" type="submit">Получить</button>--}}
                    </div>
                </div>
            @endforeach
        </div>
    </div>


@endsection



