<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BookUser;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BooksUsersController extends Controller
{
    public function create(Book $book)
    {
        return view('client.orders.create', compact('book'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $book_user = new BookUser($request->all());
        $book_user->users()->associate($request->user());
        $book_user->book_id = $request->session()->get('book_id');
        $book_user->return_date = $request->input('return_date');
        $book_user->save();
        return back()->with('success', "Заявка успешно создана!");
    }

}
