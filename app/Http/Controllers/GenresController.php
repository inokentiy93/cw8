<?php

namespace App\Http\Controllers;

use App\Models\Genre;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class GenresController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $genres= Genre::all();
        return view('client.genres.index', compact('genres'));
    }

    /**
     * @param Genre $genre
     * @return Application|Factory|View
     */
    public function show(Genre $genre)
    {
        return view('client.genres.show', compact('genre'));
    }
}
