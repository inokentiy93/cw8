<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\GenresController::class, 'index'])->name('index')->middleware('auth');

Route::prefix('client')->name('client.')->group(function () {
    Route::resource('genres', \App\Http\Controllers\GenresController::class)->only(['index', 'show']);
    Route::resource('orders', \App\Http\Controllers\BooksUsersController::class)->only('create', 'store');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
