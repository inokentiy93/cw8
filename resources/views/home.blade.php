@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">ПРИВЕТ ЧИТАТЕЛЬ</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                      Доброе пожаловать:  {{ Auth::user()->name }}
                    <br>
                        Читательский билет №: {{ Auth::user()->client_number }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
