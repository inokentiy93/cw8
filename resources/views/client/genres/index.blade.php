@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <table class="table table-striped">

                @foreach($genres as $genre)
                    <ul>
                        <li>
                            <a href="{{route('client.genres.show', ['genre' => $genre])}}">
                                <h4>{{$genre->name}}</h4>
                            </a>
                        </li>
                    </ul>
                @endforeach
            </table>
        </div>
    </div>
@endsection
