<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AuthorsTablseSeeder::class);
        $this->call(GenresTablseSeeder::class);
        $this->call(BooksTablseSeeder::class);
    }
}
