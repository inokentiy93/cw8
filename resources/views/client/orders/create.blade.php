@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col"></div>
            <div class="col">
                <br>
                <h5>Получение книги</h5>
                <b>Id книги: {{$book->id}}</b>
                <b>Название: {{$book->title}}</b>

            </div>
            <div class="col"></div>
        </div>
        <div class="row">
            <div class="col">
                <form method="post" action="{{ route('client.orders.store') }}">
                    @csrf
                    <div class="mb-3">
                        <label for="client_number">Номер читательского билета:</label>
                        <input type="text" class="form-control" name="client_number" id="client_number" value="{{ Auth::user()->client_number }}" />
                        @error('client_number')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="mb-3">
{{--                        <b>Id книги: {{$book->id}}</b>--}}
{{--                        <b>Название: {{$book->title}}</b>--}}
{{--                        <b>Автор книги: {{$book->author->name}}</b>--}}
                    </div>

                    <div class="mb-3">
                        <label for="return_date"> Дата возврата:</label>
                        <input type="text" class="date-control" name="return_date" id="return_date" value="{{ old('return_date') }}"/>
                        @error('return_date')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <br>
                    <button class="btn btn-primary" type="submit">Подать заявку</button>
                    <br>
                </form>
            </div>
        </div>
    </div>
@endsection


