<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @method user()
 */
class BookUser extends Pivot
{
    use HasFactory;

    protected $fillable = ['book_id', 'user_id', 'return_date'];

}
